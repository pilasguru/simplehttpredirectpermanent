package main

import (
	"net/http"
)

func redirectHandler(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "https://www.ejemplo.com", http.StatusMovedPermanently)
}

func main() {
	http.HandleFunc("/", redirectHandler)
	http.ListenAndServe(":1111", nil)
}
