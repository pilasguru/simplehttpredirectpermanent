# Simple HTTP Redirect Permanent

Simple GO code that stats a webserver that allways return 301 Permanent Redirect to a fix harcoded URL.

## Motivation

I have a web application with a complex URL that needs to be accessed at https://www.site.com/Site01/access. This site requires access from different domains, such as https://app.client.com, https://app.service.co, and https://files.internet.game. All of these different URLs need to be redirected to the same initial URL.

This service returns the initial URL at each request and it is included into a minimal footprint docker.

To achieve this, I use [Traefik](https://github.com/traefik/traefik), a reverse proxy and load balancer, to handle SSL negotiation and routing for the different domains.

## Getting started

Start with docker-compose:

```
docker-compose up -d
```

Send GET request to port 1111 (may be changed)

```
$ curl -I localhost:1111
HTTP/1.1 301 Moved Permanently
Content-Type: text/html; charset=utf-8
Location: https://www.ejemplo.com/
Date: Mon, 12 Jun 2023 12:01:27 GMT
```

## Traefik (optional)

Traefik is configured with these labels:

```
labels:
  - "traefik.enable=true"
  - "traefik.http.routers.myrouter.rule=Host(`app.client.com`) || Host(`app.service.co`) || Host(`files.internet.game`)"
  - "traefik.http.services.myservice.loadbalancer.server.port=80"
```

Of course, the configuration of Traefik is not explained at this document. and another proxy or solution may be implemented.

## License
This project is licensed under the MIT License.

