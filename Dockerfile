# Compilar el código Go
FROM golang:1.17 AS builder
WORKDIR /app
COPY redirect.go go.* /app/
RUN CGO_ENABLED=0 GOOS=linux go build -o app .

# Crear la imagen mínima a partir de scratch
FROM scratch
WORKDIR /app
COPY --from=builder /app/app .
EXPOSE 1111
CMD ["./app"]